var fs = require('fs')
var shell = require('shelljs')

let dir = (process.argv[2]) ? process.argv[2] : './'

if(dir == ''){
    console.log('Please indicate the folder where your .asc files are like this: node importSigs.js /home/gandalf/sigs/');
    
    process.exit()
}

fs.readdirSync(dir).forEach( file => {
    
    let command = 'gpg2 --import ' + dir + file;

    console.log('RUN: ', command);
    if (shell.exec(command).code !== 0) {
        shell.echo('Error: gpg2 import key failed')
        shell.exit(1);
    }
})