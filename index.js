var fs       = require('fs')
var readline = require('readline')
var shell    = require('shelljs')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const keyserver = 'hkp://pool.sks-keyservers.net'

var start = false
var doneCurrentKey = false
var currentKey = {"uids": []}

var LineByLineReader = require('line-by-line')
var lr = new LineByLineReader('ksp-fosdem2018.txt', {skipEmptyLines: true})


lr.on('error', err => {
    // 'err' contains error object
    console.log('Fucking err: ', err)
    
})

lr.on('line', line => {
    // cheked if we started reading the key list
    start = (!start && line == '-----BEGIN KEY LIST-----') ? true : start

    if(start){
        //if the line contains 'Fingerprint OK' then the first 3 chars are the number
        if (line.indexOf('Fingerprint OK') > 0){
            //this is the firs line for a new key
            console.log('')
            console.log('')
            console.log('')
            console.log('')
            console.log('Next!!!')
            console.log('')

            //reset current key
            currentKey = { "uids": [] }

            currentKey.no = line.substring(0, 3)
        // if the line starts with 6 spaces then this line contains the fingerprint
        } else if (line.indexOf('      ') === 0){
            currentKey.fingerprint = line.trim()
        // if the line starts with 'uid' then this line contains an uid
        } else if (line.indexOf('uid') === 0) {
            currentKey.uids.push(line)
        // if the line is '-----....' then we are done with this key
        } else if (
            line == '--------------------------------------------------------------------------------' ||
            line == '-----END KEY LIST-----'
        ){
            lr.pause()

            console.log('No: ', currentKey.no)
            console.log('Ui: ', currentKey.uids)
            
            let fingerprint = currentKey.fingerprint.replace(/\s/g, '')
            console.log('Fp: ', currentKey.fingerprint)
            console.log('')

            console.log('RUN: ', 'gpg2 --keyserver '+keyserver+' --recv-keys ' + fingerprint);
            if (shell.exec('gpg2 --keyserver '+keyserver+' --recv-keys ' + fingerprint).code !== 0) {
                shell.echo('Error: gpg2 import key failed')
                shell.exit(1);
            }
            console.log('key imported............')
            console.log('')


            console.log('RUN: ', 'gpg2 --quick-sign-key ' + fingerprint);
            if (shell.exec('gpg2 --quick-sign-key ' + fingerprint).code !== 0) {
                shell.echo('Error: gpg2 --quick-sign-key failed')
                shell.exit(1);
            }
            console.log('key signed............')
            console.log('')


            console.log('RUN: ', 'gpg2 --keyserver ' + keyserver + ' --recv-keys ' + fingerprint);
            if (shell.exec('gpg2 --keyserver '+keyserver+' --send-key ' + fingerprint).code !== 0) {
                shell.echo('Error: gpg2 --send-key failed')
                shell.exit(1);
            }
            console.log('key sent............')
            console.log('')
            console.log('')
            console.log('')

            lr.resume()
        }
    }
})

lr.on('end', () => {
    rl.close()
    console.log('')
    console.log('')
    console.log('')
    console.log('')
    console.log('')
    console.log('All done! You deserve a cookie!')  
})
